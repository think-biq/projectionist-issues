# Projectionist

Projectionist let's you use materials, projections (decals), or spot lights to conveniently render videos to geometry.
Get it now on the [Epic Games Marktplace](https://www.unrealengine.com/marketplace/projectionist).

## Issue tracking

Please use the [issue list](https://gitlab.com/think-biq/projectionist-issues/-/issues) to add or report any bug or glitch you encounter while working with projectionist.

## Support

Feel free to get in touch with us on our [discord server](https://biq.solutions/community/discord) or email to support@think-biq.com